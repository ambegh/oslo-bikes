## Description

This application fetches list of bike stations and number of available bikes.The app uses React javascript framework.

## Run the app 

* Replace client-Id in BikeStations.js file at oslo-bikes/src/components

* Install all dependecies ``` npm install ```

* Run local server ``` npm start```

* point your browser to ```localhost:3000```


## Built With

* [React](https://reactjs.org/) 


## Author

* **Aman Berhane** 

