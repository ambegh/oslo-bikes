import React from 'react';

class BikeStationHeader extends React.PureComponent {
    render() {
        const header = [
            {key: 'Name'},
            {key: 'Available bikes'},
            {key: 'Available locks'},
        ];
        
        return (
           <div className="bike-station-header">
               {header.map(head => (
                   <div key={head.key} className="bike-station-header bike-station-header-name"> {head.key} </div>
               ))}
           </div>
        );
    }
}

export default BikeStationHeader;