import React from 'react';

class BikeStation extends React.PureComponent {
    render() {
        const {station, availability} = this.props;
        const foundStation = availability.find(s => s.id === station.id) || {availability: {bikes: 'unknown', locks: 'unknown'}};

        return (
            <div className="bike-station">
                <div className="bike-station bike-station-name"> {station.title} </div>
                <div className="bike-station">{foundStation.availability.bikes} </div>
                <div className="bike-station bike-station-vaccant"> {foundStation.availability.locks} </div>
            </div>
        );
    }
}

export default BikeStation;