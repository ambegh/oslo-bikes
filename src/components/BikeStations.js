import React from 'react';
import BikeStation from './BikeStation';
import BikeStationHeader from './BikeStationHeader';
import axios from 'axios';

const basePath = 'https://oslobysykkel.no/api/v1/';
// const basePath = 'http://localhost:3004/';
const stations = 'stations';
const availability = 'stations/availability';

const clientId = 'PUT YOU CLIENT ID HERE';

class BikeStations extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            stations: [],
            availability: {},
            isLoading: true,
            error: null,
        };
    }

    componentDidMount() {
        const config = {headers: {'Client-Identifier': clientId}};
        const bikesPromise = axios.get(basePath + stations, config);
        const availabilityPromise = axios.get(basePath + availability, config);

        Promise.all([bikesPromise, availabilityPromise])
                .then(response => {
                    this.setState({
                        stations: response[0].data.stations,
                        availability: response[1].data.stations,
                        isLoading: false
                    })
                })
                .catch(error => this.setState({
                    error,
                    isLoading: false
                }));
    }

    render() {

        const {stations, availability, isLoading, error} = this.state;

        if (error) {
            return <p>{error.message}</p>;
        }

        if (isLoading) {
            return <p>Loading ...</p>;
        }
        return (
                <div className="bike-stations">
                    <BikeStationHeader/>
                    {stations.map(station => (
                            <BikeStation
                                    key={station.id}
                                    station={station}
                                    availability={availability}
                            />
                    ))}
                </div>
        );
    }
}

export default BikeStations;