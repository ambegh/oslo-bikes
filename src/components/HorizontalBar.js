import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const HorizontalBar = (props) => {
  return (
    <div className="horizontal-bar">
      <AppBar position="static">
        <Toolbar>
          <Typography variant="title" align="justify" color="inherit" className="lable">
            OSLO BYSYKKEL
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};


export default HorizontalBar;
