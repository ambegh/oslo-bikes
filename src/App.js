import React, { Component } from 'react';
import HorizontalBar from './components/HorizontalBar';
import './styles/app.css';
import BikeStations from './components/BikeStations';

class App extends Component {
  
  render() {
    return (
      <div className="app">
        <HorizontalBar />
        <BikeStations />
      </div>
    );
  }
}

export default App;
